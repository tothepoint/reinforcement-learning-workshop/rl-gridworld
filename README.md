# BOARLD

## Requirements
  - [`Python 3.x`](https://www.python.org/)
  - [`pip`](https://pypi.org/project/pip/)

## Dependencies
*Note: There is no need to install dependencies separately, the module's installation takes care of them.* 
- `progressbar2`  

## Installation
1. Clone (or download and extract) this repository
2. In your terminal, navigate to `/path/of/repo/boarld/`, which contains `setup.py`
3. Run `pip install .`

